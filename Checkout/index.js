import React from 'react';
import Modal from '../Modal';

function ModalCheckout(props) {
  const { visible, onClose } = props;

  return (
    <Modal visible={visible} onClose={onClose} size="max-w-md">
      <div className="bg-white px-8 pt-8 pb-8">
        <div className="text-left font-hind">
          <div className="w-full">
            <h2 className="text-xl leading-6 font-semibold text-gray-900 font-hind text-center">
              CHECK OUT
            </h2>
          </div>
          <div className="my-3">
            <textarea className="border border-300 px-2 py-2 w-full" placeholder="Write Description here!"  style={{ minHeight: 200 }} />
          </div>
          <div className="flex space-x-3">
            <button className="px-4 py-2 border border-300 bg-red-500 text-white" type="button" onClick={() => onClose()}>
              Cancel
            </button>
            <button className="px-4 py-2 border border-300" type="submit">
              Check Out
            </button>
          </div>
        </div>
      </div>
    </Modal>
  );
}

export default ModalCheckout;
