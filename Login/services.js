import endpoint from '../../utils/endpoint';
import axios from 'axios';

async function login({ email, password }, callback) {
  const url = endpoint.LOGIN;
  const config ={
    method: 'post',
    data: {
      email,
      password,
    },
  };

  try {
    const response = await axios(url, config);
    callback.onSuccess(response.data);
  } catch (error) {
    callback.onError(error.response);
  }
}


export default {
  Login: login,
}
