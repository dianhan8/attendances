/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/no-static-element-interactions */
/**
 *
 * Modal
 *
 */

import React from 'react';
import classnames from 'classnames';
// import styled from 'styled-components';

function Modal(props) {
  const {
    visible,
    onClose,
    children,
    size,
    rounded,
    background,
    full,
    isCloseByBackgroud,
    // ref,
    name,
  } = props;

  if (!visible) {
    return null;
  }

  const classContainer = classnames(
    'transition-all duration-300 ease z-20 overflow-y-hidden fixed bottom-0 inset-x-0 px-4 pb-4 sm:inset-0 sm:flex sm:items-center sm:justify-center overflow-x-hidden',
    {
      'visible transform opacity-100 block': visible,
      'invisible opacity-0': !visible,
      name,
    },
  );

  const classContent = classnames(
    `rounded-${rounded} overflow-x-hidden overflow-y-hidden shadow-xl transform transition-all sm:${size} sm:w-full`,
    {
      'bg-white': !background,
      background,
    },
  );

  return (
    <div className={classContainer}>
      <div
        className="fixed inset-0 transition-opacity overflow-y-hidden"
        onClick={() =>  onClose()}
      >
        <div className="absolute inset-0 bg-gray-500 opacity-75 overflow-y-hidden overflow-x-hidden" />
      </div>
      <div className={classContent}>
        {children}
        {full === false && (
          <div className="absolute right-0 top-0 mt-4 mr-4">
            <button
              type="button"
              className="focus:outline-none"
              onClick={() => onClose()}
            >
              <svg
                xmlns="http://www.w3.org/2000/svg"
                viewBox="0 0 24 24"
                width="25"
                height="25"
              >
                <path
                  className="heroicon-ui"
                  d="M16.24 14.83a1 1 0 0 1-1.41 1.41L12 13.41l-2.83 2.83a1 1 0 0 1-1.41-1.41L10.59 12 7.76 9.17a1 1 0 0 1 1.41-1.41L12 10.59l2.83-2.83a1 1 0 0 1 1.41 1.41L13.41 12l2.83 2.83z"
                />
              </svg>
            </button>
          </div>
        )}
      </div>
      {full === true && (
        <div className="absolute right-0 top-0 mt-20 mr-20">
          <button
            type="button"
            className="focus:outline-none fill-current text-white"
            onClick={() => onClose()}
          >
            <svg
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 24 24"
              width="50"
              height="50"
            >
              <path
                className="heroicon-ui"
                d="M16.24 14.83a1 1 0 0 1-1.41 1.41L12 13.41l-2.83 2.83a1 1 0 0 1-1.41-1.41L10.59 12 7.76 9.17a1 1 0 0 1 1.41-1.41L12 10.59l2.83-2.83a1 1 0 0 1 1.41 1.41L13.41 12l2.83 2.83z"
              />
            </svg>
          </button>
        </div>
      )}
    </div>
  );
}

export default Modal;
