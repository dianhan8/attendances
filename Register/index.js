import React from 'react';
import Modal from '../Modal';

function Register(props) {
  const { visible, onClose } = props;

  return (
    <Modal visible={visible} onClose={onClose} size="max-w-md">
      <div className="bg-white px-8 pt-8 pb-8">
        <div className="text-left font-hind">
          <div className="w-full">
            <h2 className="text-xl leading-6 font-semibold text-gray-900 font-hind text-center">
              Register
            </h2>
          </div>
          <div className="w-full">
            <form className="space-y-3">
              <div className="flex flex-col space-y-2">
                <span className="text-md">Email</span>
                <input className="border border-300 px-4 py-2" placeholder="email" type="email" name="email" />
              </div>

              <div className="flex flex-col space-y-2">
                <span className="text-md">Password</span>
                <input className="border border-300 px-4 py-2" type="password" name="password" placeholder="password" />
              </div>

              <div className="flex space-x-3">
                <button className="px-4 py-2 border border-300 bg-red-500 text-white" type="button" onClick={() => onClose()}>
                  Cancel
                </button>
                <button className="px-4 py-2 border border-300" type="submit">
                  Register
                </button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </Modal>
  )
}

export default Register;
