import endpoint from '../../utils/endpoint';
import axios from 'axios';

async function register({ name, email, password }, callback) {
  const url = endpoint.REGISTER;
  const config = {
    method: 'post',
    data: {
      name,
      email,
      password,
    },
  };

  try {
    const response = await axios(url, config);
    callback.onSuccess(response.data);
  } catch (error) {
    callback.onError(error.response);
  }
}

export default {
  Register: register,
};
